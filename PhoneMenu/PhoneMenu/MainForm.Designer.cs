﻿namespace PhoneMenu
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonContact = new System.Windows.Forms.Button();
            this.buttonSms = new System.Windows.Forms.Button();
            this.buttonEmail = new System.Windows.Forms.Button();
            this.buttonGallery = new System.Windows.Forms.Button();
            this.buttonWebBrowser = new System.Windows.Forms.Button();
            this.buttonCalendar = new System.Windows.Forms.Button();
            this.buttonCalculator = new System.Windows.Forms.Button();
            this.buttonConverter = new System.Windows.Forms.Button();
            this.labelTimeAndDate = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.buttonGame = new System.Windows.Forms.Button();
            this.linkLabel_CBA_RATE = new System.Windows.Forms.LinkLabel();
            this.comboBoxTitle = new System.Windows.Forms.ComboBox();
            this.buttonCurrency = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonContact
            // 
            this.buttonContact.Location = new System.Drawing.Point(13, 29);
            this.buttonContact.Name = "buttonContact";
            this.buttonContact.Size = new System.Drawing.Size(80, 23);
            this.buttonContact.TabIndex = 0;
            this.buttonContact.Text = "Contact";
            this.buttonContact.UseVisualStyleBackColor = true;
            this.buttonContact.Click += new System.EventHandler(this.buttonContact_Click);
            // 
            // buttonSms
            // 
            this.buttonSms.Location = new System.Drawing.Point(117, 29);
            this.buttonSms.Name = "buttonSms";
            this.buttonSms.Size = new System.Drawing.Size(80, 23);
            this.buttonSms.TabIndex = 1;
            this.buttonSms.Text = "Sms";
            this.buttonSms.UseVisualStyleBackColor = true;
            // 
            // buttonEmail
            // 
            this.buttonEmail.Location = new System.Drawing.Point(218, 29);
            this.buttonEmail.Name = "buttonEmail";
            this.buttonEmail.Size = new System.Drawing.Size(80, 23);
            this.buttonEmail.TabIndex = 2;
            this.buttonEmail.Text = "Email";
            this.buttonEmail.UseVisualStyleBackColor = true;
            // 
            // buttonGallery
            // 
            this.buttonGallery.Location = new System.Drawing.Point(313, 29);
            this.buttonGallery.Name = "buttonGallery";
            this.buttonGallery.Size = new System.Drawing.Size(80, 23);
            this.buttonGallery.TabIndex = 3;
            this.buttonGallery.Text = "Gallery";
            this.buttonGallery.UseVisualStyleBackColor = true;
            // 
            // buttonWebBrowser
            // 
            this.buttonWebBrowser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonWebBrowser.Location = new System.Drawing.Point(409, 29);
            this.buttonWebBrowser.Name = "buttonWebBrowser";
            this.buttonWebBrowser.Size = new System.Drawing.Size(80, 23);
            this.buttonWebBrowser.TabIndex = 4;
            this.buttonWebBrowser.Text = "WebBrowser";
            this.buttonWebBrowser.UseVisualStyleBackColor = true;
            this.buttonWebBrowser.Click += new System.EventHandler(this.buttonWebBrowser_Click);
            // 
            // buttonCalendar
            // 
            this.buttonCalendar.Location = new System.Drawing.Point(505, 29);
            this.buttonCalendar.Name = "buttonCalendar";
            this.buttonCalendar.Size = new System.Drawing.Size(80, 23);
            this.buttonCalendar.TabIndex = 5;
            this.buttonCalendar.Text = "Calendar";
            this.buttonCalendar.UseVisualStyleBackColor = true;
            this.buttonCalendar.Click += new System.EventHandler(this.buttonCalendar_Click);
            // 
            // buttonCalculator
            // 
            this.buttonCalculator.Location = new System.Drawing.Point(505, 71);
            this.buttonCalculator.Name = "buttonCalculator";
            this.buttonCalculator.Size = new System.Drawing.Size(80, 23);
            this.buttonCalculator.TabIndex = 6;
            this.buttonCalculator.Text = "Calculator";
            this.buttonCalculator.UseVisualStyleBackColor = true;
            this.buttonCalculator.Click += new System.EventHandler(this.buttonCalculator_Click);
            // 
            // buttonConverter
            // 
            this.buttonConverter.Location = new System.Drawing.Point(409, 71);
            this.buttonConverter.Name = "buttonConverter";
            this.buttonConverter.Size = new System.Drawing.Size(80, 23);
            this.buttonConverter.TabIndex = 7;
            this.buttonConverter.Text = "Converter";
            this.buttonConverter.UseVisualStyleBackColor = true;
            this.buttonConverter.Click += new System.EventHandler(this.buttonConverter_Click);
            // 
            // labelTimeAndDate
            // 
            this.labelTimeAndDate.AutoSize = true;
            this.labelTimeAndDate.Location = new System.Drawing.Point(321, 150);
            this.labelTimeAndDate.Name = "labelTimeAndDate";
            this.labelTimeAndDate.Size = new System.Drawing.Size(72, 13);
            this.labelTimeAndDate.TabIndex = 8;
            this.labelTimeAndDate.Text = "TimeAndDate";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // buttonGame
            // 
            this.buttonGame.Location = new System.Drawing.Point(313, 71);
            this.buttonGame.Name = "buttonGame";
            this.buttonGame.Size = new System.Drawing.Size(80, 23);
            this.buttonGame.TabIndex = 9;
            this.buttonGame.Text = "Game";
            this.buttonGame.UseVisualStyleBackColor = true;
            this.buttonGame.Click += new System.EventHandler(this.buttonGame_Click);
            // 
            // linkLabel_CBA_RATE
            // 
            this.linkLabel_CBA_RATE.AutoSize = true;
            this.linkLabel_CBA_RATE.Location = new System.Drawing.Point(12, 107);
            this.linkLabel_CBA_RATE.Name = "linkLabel_CBA_RATE";
            this.linkLabel_CBA_RATE.Size = new System.Drawing.Size(87, 13);
            this.linkLabel_CBA_RATE.TabIndex = 10;
            this.linkLabel_CBA_RATE.TabStop = true;
            this.linkLabel_CBA_RATE.Text = "Go To CBA Rate";
            this.linkLabel_CBA_RATE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_CBA_RATE_LinkClicked);
            // 
            // comboBoxTitle
            // 
            this.comboBoxTitle.FormattingEnabled = true;
            this.comboBoxTitle.Location = new System.Drawing.Point(12, 73);
            this.comboBoxTitle.Name = "comboBoxTitle";
            this.comboBoxTitle.Size = new System.Drawing.Size(184, 21);
            this.comboBoxTitle.TabIndex = 11;
            // 
            // buttonCurrency
            // 
            this.buttonCurrency.Location = new System.Drawing.Point(218, 70);
            this.buttonCurrency.Name = "buttonCurrency";
            this.buttonCurrency.Size = new System.Drawing.Size(80, 23);
            this.buttonCurrency.TabIndex = 12;
            this.buttonCurrency.Text = "Currency ";
            this.buttonCurrency.UseVisualStyleBackColor = true;
            this.buttonCurrency.Click += new System.EventHandler(this.buttonCurrency_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 271);
            this.Controls.Add(this.buttonCurrency);
            this.Controls.Add(this.comboBoxTitle);
            this.Controls.Add(this.linkLabel_CBA_RATE);
            this.Controls.Add(this.buttonGame);
            this.Controls.Add(this.labelTimeAndDate);
            this.Controls.Add(this.buttonConverter);
            this.Controls.Add(this.buttonCalculator);
            this.Controls.Add(this.buttonCalendar);
            this.Controls.Add(this.buttonWebBrowser);
            this.Controls.Add(this.buttonGallery);
            this.Controls.Add(this.buttonEmail);
            this.Controls.Add(this.buttonSms);
            this.Controls.Add(this.buttonContact);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonContact;
        private System.Windows.Forms.Button buttonSms;
        private System.Windows.Forms.Button buttonEmail;
        private System.Windows.Forms.Button buttonGallery;
        private System.Windows.Forms.Button buttonWebBrowser;
        private System.Windows.Forms.Button buttonCalendar;
        private System.Windows.Forms.Button buttonCalculator;
        private System.Windows.Forms.Button buttonConverter;
        private System.Windows.Forms.Label labelTimeAndDate;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button buttonGame;
        private System.Windows.Forms.LinkLabel linkLabel_CBA_RATE;
        private System.Windows.Forms.ComboBox comboBoxTitle;
        private System.Windows.Forms.Button buttonCurrency;
    }
}

