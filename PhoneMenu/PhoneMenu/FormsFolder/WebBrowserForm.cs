﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneMenu
{
    public partial class WebBrowserForm : Form
    {
        public WebBrowserForm()
        {
            InitializeComponent();
        }

        private void toolStripButtonGo_Click(object sender, EventArgs e)
        {
            webBrowser1.Navigate(toolStripTextBoxURLL.Text);
        }

        private void toolStripButtonGoBack_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void toolStripButtonGoForward_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void toolStripButtonRefresh_Click(object sender, EventArgs e)
        {
            webBrowser1.Refresh();
        }

        private void toolStripButtonStop_Click(object sender, EventArgs e)
        {
            webBrowser1.Stop();
        }

        private void toolStripButtonHome_Click(object sender, EventArgs e)
        {
            webBrowser1.GoHome();
        }
    }
}
