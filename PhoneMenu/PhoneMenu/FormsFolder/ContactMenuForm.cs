﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneMenu
{
    public partial class ContactMenuForm : Form
    {
        ContactController contact;
        AddContactForm addForm;
        public ContactMenuForm()
        {
            InitializeComponent();
            contact = new ContactController();
            this.dataGridView1.DataSource = contact.GetContact();
        }

        private void buttonAddContact_Click(object sender, EventArgs e)
        {
            contact = new ContactController();
            addForm = new AddContactForm();
            if(addForm.ShowDialog() == DialogResult.OK)
            {
                contact = new ContactController();
                string name = addForm.textBoxName.Text.ToString();
                string surname = addForm.textBoxPhone.Text.ToString();
                string phone = addForm.textBoxSurname.Text.ToString();
                string email = name + surname + "@gmail.com";
                contact.AddContact(name, surname, phone, email);

                //this.Close();
                this.dataGridView1.DataSource = null;

                this.dataGridView1.DataSource = contact.GetContact();
            }

            
            
        }

        int index = 0;
        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
              index= e.RowIndex;
            
        }
        

        private void buttonDelete_Click(object sender, EventArgs e)
        {

            DataTable items = contact.GetContact();

            string id = items.Rows[index][0].ToString();

            this.dataGridView1.DataSource = null;
            contact.DeleteContact(int.Parse(id));
            this.dataGridView1.DataSource = contact.GetContact();
        }
    }
}
