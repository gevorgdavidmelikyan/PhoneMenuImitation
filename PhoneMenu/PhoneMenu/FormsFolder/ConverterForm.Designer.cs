﻿namespace PhoneMenu
{
    partial class ConverterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxSelectList = new System.Windows.Forms.ComboBox();
            this.comboBoxArg1 = new System.Windows.Forms.ComboBox();
            this.comboBoxArg2 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxInsert = new System.Windows.Forms.TextBox();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // comboBoxSelectList
            // 
            this.comboBoxSelectList.FormattingEnabled = true;
            this.comboBoxSelectList.Location = new System.Drawing.Point(13, 13);
            this.comboBoxSelectList.Name = "comboBoxSelectList";
            this.comboBoxSelectList.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSelectList.TabIndex = 0;
            this.comboBoxSelectList.SelectedIndexChanged += new System.EventHandler(this.comboBoxSelectList_SelectedIndexChanged_1);
            // 
            // comboBoxArg1
            // 
            this.comboBoxArg1.FormattingEnabled = true;
            this.comboBoxArg1.Location = new System.Drawing.Point(13, 40);
            this.comboBoxArg1.Name = "comboBoxArg1";
            this.comboBoxArg1.Size = new System.Drawing.Size(121, 21);
            this.comboBoxArg1.TabIndex = 1;
            this.comboBoxArg1.SelectedIndexChanged += new System.EventHandler(this.comboBoxArg1_SelectedIndexChanged_1);
            // 
            // comboBoxArg2
            // 
            this.comboBoxArg2.FormattingEnabled = true;
            this.comboBoxArg2.Location = new System.Drawing.Point(12, 67);
            this.comboBoxArg2.Name = "comboBoxArg2";
            this.comboBoxArg2.Size = new System.Drawing.Size(121, 21);
            this.comboBoxArg2.TabIndex = 2;
            this.comboBoxArg2.SelectedIndexChanged += new System.EventHandler(this.comboBoxArg2_SelectedIndexChanged_1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(57, 95);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxInsert
            // 
            this.textBoxInsert.Location = new System.Drawing.Point(153, 40);
            this.textBoxInsert.Name = "textBoxInsert";
            this.textBoxInsert.Size = new System.Drawing.Size(100, 20);
            this.textBoxInsert.TabIndex = 4;
            // 
            // textBoxResult
            // 
            this.textBoxResult.Location = new System.Drawing.Point(153, 68);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.Size = new System.Drawing.Size(100, 20);
            this.textBoxResult.TabIndex = 5;
            // 
            // ConverterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.textBoxInsert);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBoxArg2);
            this.Controls.Add(this.comboBoxArg1);
            this.Controls.Add(this.comboBoxSelectList);
            this.Name = "ConverterForm";
            this.Text = "ConverterForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxSelectList;
        private System.Windows.Forms.ComboBox comboBoxArg1;
        private System.Windows.Forms.ComboBox comboBoxArg2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxInsert;
        private System.Windows.Forms.TextBox textBoxResult;
    }
}