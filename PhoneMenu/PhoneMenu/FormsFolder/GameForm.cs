﻿using PhoneMenu.GameFolder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneMenu.FormsFolder
{
    public partial class GameForm : Form
    {
        Road road;
        Car car;
        public GameForm()
        {
            InitializeComponent();
            road = new Road(this);
            car = new Car(this, road, 90, 70);
        }

        private void GameForm_Paint(object sender, PaintEventArgs e)
        {

            road.Paint();
            car.Paint();
        }

        private void GameForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
                car.MoveLeft();
            if (e.KeyCode == Keys.Right)
                car.MoveRigth();
        }
    }
}
