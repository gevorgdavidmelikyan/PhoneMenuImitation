﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneMenu
{
    public partial class ConverterForm : Form
    {
        Dictionary<string, double> lengths;
        Dictionary<string, double> weigth;
        string argument1;
        string argument2;
        double value;
        double result;
        public ConverterForm()
        {
            InitializeComponent();
            GenerateColection();
            comboBoxSelectList.Items.Add("heigth");
            comboBoxSelectList.Items.Add("weigth");

        }
        public void GenerateColection()
        {

            lengths = new Dictionary<string, double>();
            weigth = new Dictionary<string, double>();

            lengths.Add("mm", 1);
            lengths.Add("sm", 10);
            lengths.Add("inch", 25.4);
            lengths.Add("m", 1000);
            lengths.Add("km", 1000000);

            weigth.Add("gr", 1);
            weigth.Add("kg", 1000);
            weigth.Add("ton", 1000000);
            weigth.Add("funt", 453.592);
            weigth.Add("unc", 28.3495);
        }

        

        

        private void textBoxInsert_TextChanged(object sender, EventArgs e)
        {
        }

        private void textBoxResult_TextChanged(object sender, EventArgs e)
        {
        }

        

        private void comboBoxSelectList_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (
          comboBoxSelectList.Text == "heigth")
            {
                comboBoxArg1.Items.Clear();
                comboBoxArg2.Items.Clear();
                foreach (var item in lengths)
                {
                    comboBoxArg1.Items.Add(item.Key);
                    comboBoxArg2.Items.Add(item.Key);
                }
            }
            else if (comboBoxSelectList.Text == "weigth")
            {
                comboBoxArg1.Items.Clear();
                comboBoxArg2.Items.Clear();
                foreach (var item in weigth)
                {
                    comboBoxArg1.Items.Add(item.Key);
                    comboBoxArg2.Items.Add(item.Key);
                }
            }
        }

        private void comboBoxArg1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            argument1 = comboBoxArg1.Text;
        }

        private void comboBoxArg2_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            argument2 = comboBoxArg2.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            value = Convert.ToDouble(textBoxInsert.Text);

            switch (comboBoxSelectList.Text)
            {
                case "heigth":
                    if (lengths[argument1] < lengths[argument2])
                    {
                        result = ((lengths[argument1] / lengths[argument2]) * value);
                    }
                    else
                    {
                        result = (1 / (lengths[argument2] / lengths[argument1]) * value);
                    }
                    break;
                case "weigth":
                    if (weigth[argument1] < weigth[argument2])
                    {
                        result = ((weigth[argument1] / weigth[argument2]) * value);
                    }
                    else
                    {
                        result = (1 / (weigth[argument2] / weigth[argument1]) * value);
                    }
                    break;
                default:
                    break;
            }



            textBoxResult.Text = result.ToString();
            textBoxResult.Show();

        }
    }
}
