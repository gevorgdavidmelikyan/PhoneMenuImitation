﻿using PhoneMenu.CBA_ServiceReference;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneMenu
{
    public partial class CurrencyConverterForm : Form
    {
        public CurrencyConverterForm()
        {
            InitializeComponent();
            Getdata();
        }

        private void Getdata()
        {
            GateSoapClient client = new GateSoapClient();
            ExchangeRates exchange = client.ExchangeRatesByDate(DateTime.Now);
            foreach (ExchangeRate rate in exchange.Rates)
            {
                string fullinfo = rate.Amount + rate.ISO + "-" + rate.Rate;
                comboBoxFrom.Items.Add(fullinfo);
                comboBoxTo.Items.Add(fullinfo);

            }
            comboBoxFrom.SelectedIndex = 0;
            comboBoxTo.SelectedIndex = 1;
        }
        public void Convert()
        {
        }
    }
}
