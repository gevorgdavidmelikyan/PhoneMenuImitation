﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneMenu
{
    public partial class CalendarForm : Form
    {
        public CalendarForm()
        {
            InitializeComponent();
            timer1.Start();
            this.labelTimeAndDate.Text = DateTime.Now.ToString();
            
        }
        
        private void timer1_Tick_1(object sender, EventArgs e)
        {

            this.labelTimeAndDate.Text = DateTime.Now.ToString();
            timer1.Start();
        }
    }
}
