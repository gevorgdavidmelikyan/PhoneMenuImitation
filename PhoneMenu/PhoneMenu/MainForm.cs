﻿using PhoneMenu.FormsFolder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace PhoneMenu
{
    public partial class MainForm : Form
    {
        ContactMenuForm contactMenu;
        string[] rssData = null;
        
        public MainForm()
        {
            InitializeComponent();

            RegionInfo regionInfo = RegionInfo.CurrentRegion;
            CultureInfo cultureInfo = CultureInfo.CurrentCulture;
            timer1.Start();
            rssData = GetRssData("https://www.cba.am/_layouts/rssreader.aspx?rss=280F57B8-763C-4EE4-90E0-8136C13E47DA");
            comboBoxTitle.Items.Clear();
            for (int i = 0; i < rssData.GetLength(0); i++)
            {
                if (rssData[i] != null)
                    comboBoxTitle.Items.Add(rssData[i]);
                comboBoxTitle.SelectedIndex = 0;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            labelTimeAndDate.Text = DateTime.Now.ToString("F");
            timer1.Start();
        }

        private void buttonContact_Click(object sender, EventArgs e)
        {
            contactMenu = new ContactMenuForm();
            contactMenu.Show();
        }

        private void buttonCalendar_Click(object sender, EventArgs e)
        {
            CalendarForm calendar = new CalendarForm();
            calendar.Show();
        }

        private void buttonCalculator_Click(object sender, EventArgs e)
        {
            CalcForm calc = new CalcForm();
            calc.Show();
        }

        private void buttonWebBrowser_Click(object sender, EventArgs e)
        {
            WebBrowserForm explorer = new WebBrowserForm();
            explorer.Show();
        }

        private void buttonConverter_Click(object sender, EventArgs e)
        {
            ConverterForm con = new ConverterForm();
            con.Show();
        }
        private string[] GetRssData(string chanel)
        {
            WebRequest myRequest = WebRequest.Create(chanel);
            WebResponse myResponse = myRequest.GetResponse();
            Stream rssStream = myResponse.GetResponseStream();
            XmlDocument rssDoc = new XmlDocument();
            rssDoc.Load(rssStream);
            XmlNodeList rssItems = rssDoc.SelectNodes("rss/channel/item");
            string[] tempRssData = new string[60];
            for (int i = 0; i < rssItems.Count; i++)
            {
                XmlNode rssNode;
                rssNode = rssItems.Item(i).SelectSingleNode("title");
                if (rssNode != null)
                    tempRssData[i] = rssNode.InnerText;
                else
                    tempRssData[i] = "";
            }
            return tempRssData;
        }

        private void linkLabel_CBA_RATE_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //if (rssData[comboBoxTitle.SelectedIndex] != null) ;
            // Process.Start(rssData[comboBoxTitle.SelectedIndex]);

            e.Link.Visited = true;
            Process.Start("https://www.cba.am/am/SitePages/ExchangeArchive.aspx");
        }

        private void buttonCurrency_Click(object sender, EventArgs e)
        {
            CurrencyConverterForm currency = new CurrencyConverterForm();
            currency.Show();
        }

        private void buttonGame_Click(object sender, EventArgs e)
        {
            GameForm game = new GameForm();
            game.Show();
        }
    }
}
