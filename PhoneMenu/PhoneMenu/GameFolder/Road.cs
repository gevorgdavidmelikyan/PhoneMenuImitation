﻿using PhoneMenu.FormsFolder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneMenu.GameFolder
{
    class Road
    {
        Graphics graph;
        int x;
        int y;
        int w;
        GameForm fr;

        public Road(GameForm fr)
        {
            this.fr = fr;
            this.graph = fr.CreateGraphics();
            X = fr.Width / 2 - 150;
            W = fr.Width / 2;
            Y = 0;
        }

        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }
        public int W { get => w; set => w = value; }

        public void Paint()
        {
            graph.FillRectangle(Brushes.Black, X, Y, W, fr.Height);
        }
    }
}
