﻿using PhoneMenu.FormsFolder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PhoneMenu.GameFolder
{
    class Car

    {
        protected Road road;
        protected Graphics g;
        protected GameForm fr;
        protected Image img;
        protected Rectangle rect;
        protected int x;
        protected int y;
        protected int w;
        protected int h;
        public Car(GameForm fr, Road r, int h, int w)
        {
            this.fr = fr;
            this.road = r;
            this.h = h;
            this.w = w;
            img = new Bitmap(@"jpg\f1.png");
            this.y = (int)(fr.Height - 1.8 * h);
            this.x = (int)(road.X + road.W - 1.2 * w);
            g = fr.CreateGraphics();
            rect = new Rectangle(x, y, w, h);
            Thread.Sleep(20);
        }

        public void Paint()
        {

            g.DrawImage(img, rect);

            //g.FillRectangle(Brushes.Red, rect);
            //g.DrawImage(img, x, y, w, h);

        }

        public void MoveLeft()
        {
            rect.X -= w;
            fr.Invalidate();
        }
        public void MoveRigth()
        {
            rect.X += w;
            fr.Invalidate();
        }
    }
}
